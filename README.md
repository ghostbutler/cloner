Cloner
======

Description
-----------

This tool is made to pull easily all projects with their tree to current folder.

Usage
-----

Once you have pull this repo, put the main.go where you want the projects to be
cloned to.

Then run:

```bash
go run clone.go
```

If there are no error, you should find the project into **./ghostbutler/** directory.

To pull every projects to master state, copy the pull.go file into project root,
and run:

```bash
go run pull.go
```

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/ghostbutler/cloner.git
