#!/bin/bash

# rabbitmq library
go get github.com/streadway/amqp

# sound lib (faiface)
go get github.com/faiface/beep
go get github.com/hajimehoshi/go-mp3
go get github.com/hajimehoshi/oto

# redis libraries
go get github.com/go-redis/redis
go get github.com/bsm/redis-lock

# ghost butler libraries
go get gitlab.com/ghostbutler/tool/service
go get gitlab.com/ghostbutler/tool/signal
go get gitlab.com/ghostbutler/tool/crypto

# machine id
go get github.com/denisbrodbeck/machineid

