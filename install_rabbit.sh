#!/bin/bash

# RabbitMQ installer for ubuntu bionic distribution (18.04)
# Must be run as root
# Steps come from https://www.rabbitmq.com/install-debian.html

# clean deb file
> /etc/apt/sources.list.d/rabbitmq.list

# add sources
echo "deb http://dl.bintray.com/rabbitmq-erlang/debian bionic erlang" > /etc/apt/sources.list.d/rabbitmq.list
echo "deb https://dl.bintray.com/rabbitmq/debian bionic main" > /etc/apt/sources.list.d/rabbitmq.list

# add signing key
wget -O - "https://github.com/rabbitmq/signing-keys/releases/download/2.0/rabbitmq-release-signing-key.asc" | sudo apt-key add -

# install erlang
apt install -y erlang-nox
apt install -y rabbitmq-server
