package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

const (
	// gitlab URL
	URL = "https://gitlab.com/api/v4"

	// root group
	GroupName = "ghostbutler"
)

type Project struct {
	Id       int    `json:"id"`
	Name     string `json:"name"`
	FullPath string `json:"path_with_namespace"`
	PullURL  string `json:"http_url_to_repo"`
	WebUrl   string `json:"web_url"`
}

type Group struct {
	Id       int    `json:"id"`
	WebURL   string `json:"web_url"`
	Name     string `json:"name"`
	Path     string `json:"path"`
	FullPath string `json:"full_path"`
}

func extractGroup(gitlabUrl string,
	identifier interface{},
	projectList map[int]*Project) error {
	// extract subgroups
	var url string
	switch identifier.(type) {
	case string:
		url = gitlabUrl + "/groups/" +
			identifier.(string) +
			"/subgroups"
		break
	case int:
		url = gitlabUrl + "/groups/" +
			strconv.Itoa(identifier.(int)) +
			"/subgroups"
		break
	}
	request, _ := http.NewRequest("GET",
		url,
		nil)

	// iterate sub groups
	if response, err := http.DefaultClient.Do(request); err == nil {
		content, _ := ioutil.ReadAll(response.Body)
		_ = response.Body.Close()
		var subGroupList []Group
		if err := json.Unmarshal(content,
			&subGroupList); err == nil {
			for _, subGroup := range subGroupList {
				fmt.Println("now entering", subGroup.FullPath)
				if err := extractGroup(gitlabUrl,
					subGroup.Id,
					projectList); err != nil {
					return err
				}
			}
		} else {
			return err
		}
	} else {
		return err
	}

	// iterate projects
	url = strings.Replace(url,
		"subgroups",
		"projects",
		1)
	request, _ = http.NewRequest("GET",
		url,
		nil)
	if response, err := http.DefaultClient.Do(request); err == nil {
		content, _ := ioutil.ReadAll(response.Body)
		_ = response.Body.Close()
		var subProject []*Project
		if err := json.Unmarshal(content,
			&subProject); err == nil {
			for _, project := range subProject {
				projectList[project.Id] = project
				fmt.Println("found",
					project.Name,
					"("+
						project.FullPath+
						")")
			}
		}
	} else {
		return err
	}
	return nil
}

func Extract(gitlabUrl string,
	groupName string) (map[int]*Project, error) {
	projectList := make(map[int]*Project)
	err := extractGroup(gitlabUrl,
		groupName,
		projectList)
	return projectList, err
}

func Clone(project *Project) error {
	if err := os.MkdirAll(project.FullPath,
		0774); err == nil {
		if err = exec.Command("git",
			"clone",
			project.PullURL,
			project.FullPath).Run(); err == nil {
			return nil
		} else {
			return err
		}
	} else {
		return err
	}
}

func Pull(project *Project) error {
	directory := "./" +
		strings.TrimPrefix(project.FullPath,
			strings.Split(project.FullPath,
				"/")[0]+"/")
	if _, err := os.Stat(directory); os.IsNotExist(err) {
		fmt.Println("project is missing, now cloning it")
		if err := Clone(project); err == nil {
			return nil
		} else {
			return err
		}
	} else {
		fmt.Println("entering directory",
			directory)
		if err = exec.Command("git",
			"-C",
			directory,
			"pull").Run(); err == nil {
			return nil
		} else {
			return err
		}
	}

}

func main() {
	// extract recursively projects list
	fmt.Println("now looking for projects...")
	if projectList, err := Extract(URL,
		GroupName); err == nil {
		// pull found projects
		fmt.Println("now pulling projects...")
		for _, project := range projectList {
			fmt.Println("pulling",
				project.PullURL)
			if err := Pull(project); err != nil {
				fmt.Println("failed with error",
					err)
				os.Exit(1)
			}
		}
	} else {
		// failed to find projects
		fmt.Println(err)
		os.Exit(1)
	}
}
